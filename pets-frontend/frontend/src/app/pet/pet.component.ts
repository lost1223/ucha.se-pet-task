import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {MessageService} from 'primeng/api';

export interface Breed {
  name?: string,
  value?: string
}

@Component({
  selector: 'se-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.scss'],
  providers: [MessageService]
})
export class PetComponent implements OnInit {

  addPetView: boolean = false;
  findPetView: boolean = false;
  public breeds: Breed[] = new Array();
  breed: Breed = {};
  petName: string = '';
  petDescription: string = '';
  result: { name: '', breed: '', description: '', recordDate: '' }[] = new Array();
  item: any;

  constructor(private http: HttpClient,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.breeds = [
      {name: 'Cat', value: 'cat'},
      {name: 'Dog', value: 'dog'},
      {name: 'Dinosaur', value: 'dinosaur'},
      {name: 'Parrot', value: 'parrot'},
      {name: 'Wolf', value: 'wolf'}
    ]
  }

  showViewAddPet() {
    this.addPetView = true;
    this.findPetView = false;
  }

  showViewfindPet() {
    this.findPetView = true;
    this.addPetView = false;
  }

  savePet() {
    let params = {
      name: this.petName,
      breed: this.breed.value,
      description: this.petDescription
    };

    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {headers: headers};


    this.http.post('http://localhost:8000/api/v1/pets', params, options).subscribe(
      data => console.log('update view with ' + data)
      , error => this.messageService.add({severity: 'error', summary: 'Service', detail: error.message}));
  }

  findPet() {
    this.http.get('http://localhost:8000/api/v1/pets/search',
      {
        params: new HttpParams()
          .set('name', this.petName ? this.petName : '')
          .set('breed', <string>this.breed.value ? <string>this.breed.value : ''),
        headers: new HttpHeaders()
      }).subscribe((data: any) => {
      if (data.status === 'not_found') {
        this.result = [];
        return;
      }
      this.result = data;
    })
      ,
      (error: any) => {
        this.messageService.add({severity: 'error', summary: 'Service', detail: error.message})
      };
  }

  highlightName() {
    if (this.petName) {
      return {'background-color': 'yellow'}
    }
    return {};
  }

  highlightBreed() {
    if (this.breed.value !== '' && this.breed.value !== undefined) {
      return {'background-color': 'green'}
    }
    return {};
  }
}
