<?php

namespace ContainerQVS32lA;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder1801a = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializere81e5 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesedf20 = [
        
    ];

    public function getConnection()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getConnection', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getMetadataFactory', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getExpressionBuilder', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'beginTransaction', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->beginTransaction();
    }

    public function getCache()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getCache', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getCache();
    }

    public function transactional($func)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'transactional', array('func' => $func), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->transactional($func);
    }

    public function commit()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'commit', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->commit();
    }

    public function rollback()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'rollback', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getClassMetadata', array('className' => $className), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'createQuery', array('dql' => $dql), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'createNamedQuery', array('name' => $name), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'createQueryBuilder', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'flush', array('entity' => $entity), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'clear', array('entityName' => $entityName), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->clear($entityName);
    }

    public function close()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'close', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->close();
    }

    public function persist($entity)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'persist', array('entity' => $entity), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'remove', array('entity' => $entity), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'refresh', array('entity' => $entity), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'detach', array('entity' => $entity), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'merge', array('entity' => $entity), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getRepository', array('entityName' => $entityName), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'contains', array('entity' => $entity), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getEventManager', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getConfiguration', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'isOpen', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getUnitOfWork', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getProxyFactory', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'initializeObject', array('obj' => $obj), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'getFilters', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'isFiltersStateClean', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'hasFilters', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return $this->valueHolder1801a->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializere81e5 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder1801a) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder1801a = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder1801a->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, '__get', ['name' => $name], $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        if (isset(self::$publicPropertiesedf20[$name])) {
            return $this->valueHolder1801a->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1801a;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder1801a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1801a;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder1801a;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, '__isset', array('name' => $name), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1801a;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder1801a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, '__unset', array('name' => $name), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1801a;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder1801a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, '__clone', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        $this->valueHolder1801a = clone $this->valueHolder1801a;
    }

    public function __sleep()
    {
        $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, '__sleep', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;

        return array('valueHolder1801a');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializere81e5 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializere81e5;
    }

    public function initializeProxy() : bool
    {
        return $this->initializere81e5 && ($this->initializere81e5->__invoke($valueHolder1801a, $this, 'initializeProxy', array(), $this->initializere81e5) || 1) && $this->valueHolder1801a = $valueHolder1801a;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder1801a;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder1801a;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
