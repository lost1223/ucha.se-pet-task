<?php

namespace Symfony\Config\Security\ProviderConfig;


use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\Loader\ParamConfigurator;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class ChainConfig
{
    private $providers;

    public function __construct(array $value = [])
    {

        if (isset($value['providers'])) {
            $this->providers = $value['providers'];
            unset($value['providers']);
        }

        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__) . implode(', ', array_keys($value)));
        }
    }

    /**
     * @param ParamConfigurator|list<mixed|ParamConfigurator> $value
     * @return $this
     */
    public function providers($value): self
    {
        $this->providers = $value;

        return $this;
    }

    public function toArray(): array
    {
        $output = [];
        if (null !== $this->providers) {
            $output['providers'] = $this->providers;
        }

        return $output;
    }


}
