<?php

namespace Symfony\Config\Security\FirewallConfig;


use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\Loader\ParamConfigurator;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class HttpBasicConfig
{
    private $provider;
    private $realm;

    public function __construct(array $value = [])
    {

        if (isset($value['provider'])) {
            $this->provider = $value['provider'];
            unset($value['provider']);
        }

        if (isset($value['realm'])) {
            $this->realm = $value['realm'];
            unset($value['realm']);
        }

        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__) . implode(', ', array_keys($value)));
        }
    }

    /**
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function provider($value): self
    {
        $this->provider = $value;

        return $this;
    }

    /**
     * @default 'Secured Area'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function realm($value): self
    {
        $this->realm = $value;

        return $this;
    }

    public function toArray(): array
    {
        $output = [];
        if (null !== $this->provider) {
            $output['provider'] = $this->provider;
        }
        if (null !== $this->realm) {
            $output['realm'] = $this->realm;
        }

        return $output;
    }


}
